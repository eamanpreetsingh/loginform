import { ActivatedRoute, NavigationEnd, NavigationStart, Router, Event } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'login-web';
  val: any;
  spinner:any;

  constructor(private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe((value) => {
      this.getting(value);
    });
  }
  getting(values: any) {
    this.val = values.bool;
  }
}
