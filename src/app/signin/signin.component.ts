import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validator, RequiredValidator, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  formData: FormGroup;
  store?: string;
  i: number = 0;
  disabled = false;
  bool: any;
  getvalue: any;
  strvalue: any;
  mail?: string;
  pw: any;
  signinemail?: string;
  signinpw: any;
  t=0;

  constructor(private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService) {
    this.formData = fb.group({
      email: new FormControl("", [Validators.required, Validators.minLength(5)]),
      password: new FormControl("", [Validators.required])
    });
  }

  ngOnInit(): void { }

  formSubmit(formData: any) {
    this.signinemail = formData.value.email;
    this.signinpw = formData.value.password;

    for (this.i = 0; this.i < 2; this.i++) {
      this.getvalue = localStorage.getItem(JSON.stringify(this.i));
      this.strvalue = JSON.parse(this.getvalue);
      this.mail = this.strvalue.email;
      this.pw = this.strvalue.password;

      if (this.mail == this.signinemail && this.pw == this.signinpw) {
        this.bool = JSON.stringify(this.disabled);
        console.log("signined");
        this.toastr.success("login Successfully","Welcome User");
        this.router.navigate([""], { queryParams: { bool: this.bool } });
        this.t=1;
        break;
      }
      else {
        console.log("error");
      }

    }

    if(this.t==0){
      this.toastr.error("Failed","Invalid Username or Password");
    }

  }

}
