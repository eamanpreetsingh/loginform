import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {


  formData: FormGroup;
  store?: string;
  i = 0;
  j: any;
  t = 0;
  getvalue: any;
  strvalue: any;
  mail?: string;
  pw?: string;
  phn?: number;
  nam?: string;
  loop: any;

  constructor(private fb: FormBuilder) {
    this.formData = fb.group({
      email: new FormControl("", [Validators.required, Validators.minLength(5)]),
      password: new FormControl("", [Validators.required]),
      phoneno: new FormControl("", [Validators.required,]),
      name: new FormControl("", [Validators.required])
    });
  }

  ngOnInit(): void { }

  formSubmit(formData: any) {
    console.log(formData.value.email.length);
    // this.signdetail.getdata().subscribe((value)=>{
    // this.loop=value+1});
    // if(this.loop==undefined){ this.loop=1;}
    // else{console.log("dnknk")}
    this.loop = localStorage.getItem("loop");
    this.loop = JSON.stringify(this.loop);
    if (this.loop == "null") {
      this.loop = 0;
    }
    else {
      console.log("sgtmgk", this.loop);
    }
    console.log(this.loop);
    for (this.j = 0; this.j <= this.loop; this.j++) {
      this.getvalue = localStorage.getItem(JSON.stringify(this.j));
      console.log(this.getvalue);
      if (this.getvalue != null) {
        this.strvalue = JSON.parse(this.getvalue);
        this.mail = this.strvalue.email;
        this.pw = this.strvalue.password;
        this.nam = this.strvalue.name;
        this.phn = this.strvalue.phoneno;

        if (this.mail == formData.value.email && this.pw == formData.value.password && this.phn == formData.value.phoneno) {
          console.log("value already exists");
          this.t = 1;
          break;
        }
        else if (this.t == 0 && this.j== this.loop) {
          this.store = formData.value;
          localStorage.setItem(JSON.stringify(this.i), JSON.stringify(this.store));
          console.log(this.store);
          this.i+=1;
        }
        else {
          continue;
        }

      }
      else {
        this.store = formData.value;
        localStorage.setItem(JSON.stringify(this.i), JSON.stringify(this.store));
        console.log(this.store,this.i);
        this.i+=1;
        break;
      }
    }

    // if(this.check==1){
    // this.i+=1;
    // }
    localStorage.setItem("loop", JSON.stringify(this.i)); 

  }

}
