import { SigninphnComponent } from './signinphn/signinphn.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: "signup", component:SignupComponent },
  {path: "signin", component:SigninComponent},
  {path: "signinphn", component:SigninphnComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
