import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SigninphnComponent } from './signinphn.component';

describe('SigninphnComponent', () => {
  let component: SigninphnComponent;
  let fixture: ComponentFixture<SigninphnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SigninphnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninphnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
