import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signinphn',
  templateUrl: './signinphn.component.html',
  styleUrls: ['./signinphn.component.css']
})
export class SigninphnComponent implements OnInit {

  formData: FormGroup;
  otpData: FormGroup;
  signinphoneno?: number;
  signinname?: string;
  t = 0;
  store?: string;
  i: number = 0;
  disabled = false;
  bool: any;
  getvalue: any;
  strvalue: any;
  nam?: string;
  phn: any;
  no?: number;
  first?: number;
  second?: number;
  third?: number;
  fourth?: number;
  n = 0;
  mod1?: number;
  mod2?: number;
  mod3?: number;
  mod4?: number;
  value: any;
  element: any;

  constructor(private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService) {
    this.formData = fb.group({
      phoneno: new FormControl("", [Validators.required, Validators.minLength(5)]),
      name: new FormControl("", [Validators.required])
    });
    this.otpData = fb.group({
      first: new FormControl("", [Validators.required]),
      second: new FormControl("", [Validators.required]),
      third: new FormControl("", [Validators.required]),
      fourth: new FormControl("", [Validators.required])
    })
  }

  ngOnInit(): void { }

  formSubmit(formData: any) {
    this.signinphoneno = formData.value.phoneno;
    this.signinname = formData.value.name;

    for (this.i = 0; this.i < 2; this.i++) {
      this.getvalue = localStorage.getItem(JSON.stringify(this.i));
      this.strvalue = JSON.parse(this.getvalue);
      this.phn = this.strvalue.phoneno;
      this.nam = this.strvalue.name;

      if (this.nam == this.signinname && this.phn == this.signinphoneno) {
        // this.bool = JSON.stringify(this.disabled);
        // console.log("signined");
        // this.toastr.success("login Successfully","Welcome User");
        // this.router.navigate([""], { queryParams: { bool: this.bool } });
        // this.t=1;
        // break;
        this.no = Math.floor(Math.random() * 10000);
        console.log(this.no);
        localStorage.setItem("otp", JSON.stringify(this.no));
        this.value=document.getElementsByClassName("formdiv");
        this.value[0].style.display="none";
        this.element=document.getElementsByClassName("otpdiv");
        this.element[0].style.display="initial";
        this.t=1;
      }
      else {
        console.log("error");
      }

    }

    if (this.t == 0) {
      this.toastr.error("Failed", "Invalid Username or Password");
    }

  }

  // otp data function
  otpSubmit(otpData: any) {
    this.first = otpData.value.first;
    this.second = otpData.value.second;
    this.third = otpData.value.third;
    this.fourth = otpData.value.fourth;
    this.value = localStorage.getItem("otp");
    this.value = JSON.parse(this.value);
    console.log(this.value);
    this.mod4 = this.value % 10;
    this.value = Math.floor(this.value / 10);
    this.mod3 = this.value % 10;
    this.value = Math.floor(this.value / 10);
    this.mod2 = this.value % 10;
    this.value = Math.floor(this.value / 10);
    console.log(this.mod4, "jhjj", this.mod2);
    this.mod1 = this.value % 10;
    if (this.mod4 == this.fourth && this.mod3 == this.third && this.mod2 == this.second && this.mod1 == this.first) {
      this.bool = JSON.stringify(this.disabled);
      console.log("signined");
      this.toastr.success("login Successfully", "Welcome User");
      this.router.navigate([""], { queryParams: { bool: this.bool } });
      this.t = 1;
    }
    else {
      this.toastr.error("Wrong OTP", "Try again");
    }
    localStorage.removeItem("otp");
  }

  //resend otp
  resend(){
    this.no = Math.floor(Math.random() * 10000);
    console.log(this.no);
    localStorage.setItem("otp", JSON.stringify(this.no));
}

  otpfun(current:any,next:any){
    if(current.value.length){
      this.value=document.getElementsByClassName(next);
      this.value.focus();
      console.log("fnjfnf"); 
     }
     else{
       console.log("skndk");
     }
  }
}

